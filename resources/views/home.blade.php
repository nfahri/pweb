<!DOCTYPE html>
<html>
<head>
	<title>Register Form</title>
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/home.css">
	<script type="text/javascript" src="Assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="Assets/js/jquery.js"></script>
	<script type="text/javascript" src="Assets/js/home.js"></script>
</head>
<body>
<div class="container-fluid" id="atas">
	<h1>Nature Guardian</h1>
	<img id="logo" src="Assets/img/itb.png">
	<h2>Welcome</h2>
	<a href="#nav"><img id="arrow" src="Assets/img/itb.png"></a>
</div>
<div class="container-fluid" id="bawah">
	<nav class="navbar navbar-default" id="nav">
		<div class="container-fluid">
			<div class="navbar-header">
		      <a class="navbar-brand" href="#">Nature Guardian</a>
		    </div>
			<div>
				<ul class="nav navbar-nav">
					<li><a href="#">HOME</a></li>
					<li><a href="#">FORUM</a></li>
					<li><a href="#">TIPS n TRICK</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="col-sm-1" style="rgba(0,0,255,0.3)" id="kiri"></div>
	<div class="col-sm-10">
		<div class="container-fluid">
		<div class="col-sm-4" id="desc-left"></div>
			<div class="col-sm-4" id="deskripsi">
				<h2>Description of website</h2>
					<h3>Perjuangan Cinta Seorang Pujangga</h3>
					<strong>
					Tiap hari ia datang kemari <br>
					Bahkan badai saljupun tidak menghalangi <br>
					Demi menemui dambaan hati <br>
					Yang sedang duduk diam menanti<br>
					Berharap bertemu pujaan hati<br>
					</strong>
			</div>
		<div class="col-sm-4"></div>
		</div>
		<div class="container-fluid">
			<div class="col-sm-4">
				<img id="logoleft" src="Assets/img/itb.png">
			</div>
			<div class="col-sm-4" id="motto">
			<strong>
				Betapa keras hidup ini <br>
				Lelah aku mencari <br>
				Sesuatu yang pasti <br>
				Namun lelah hati ini<br>
			</strong>
			</div>
			<div class="col-sm-4">
				<img src="Assets/img/itb.png" id="logoright">
			</div>
		</div>
	</div>
	<div class="col-sm-1" style="rgba(255,0,0,0.3)" id="kanan"></div>
</div>
</body>
</html>