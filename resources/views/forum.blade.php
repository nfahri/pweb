<html>
<head>
	<title>Register Form</title>
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/forum.css">
	<script type="text/javascript" src="Assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="Assets/js/jquery.js"></script>
	<script type="text/javascript" src="Assets/js/forum.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div>
		<ul class="nav navbar-nav">
			<li><a href="#">HOME</a></li>
			<li><a href="#">FORUM</a></li>
			<li><a href="#">TIPS n TRICK</a></li>
		</ul>
	</div>
</nav>
<div class="row">
	<div class="col-sm-12" id="header-img">
		<div class="col-sm-12" id="search-div">
			<input type="search" name="search" placeholder="Search" id="box">
		</div>
	</div>
</div>
<div class="container" id="content" style="padding-top: 5%;">
	<div class="col-sm-8" id="left">
		<div id="comment">
			<div class="col-sm-2" id="logo">
				<img src="Assets/img/itb.png" id="avatar">
			</div>
			<div class="col-sm-10">
				<div class="row" id="atas">Judul trit</div>
				<div class="row" id="bawah">deskripsi</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4" id="right">
		
	</div>
</div>
</body>
</html>