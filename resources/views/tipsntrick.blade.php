<!DOCTYPE html>
<html>
<head>
<title>Register Form</title>
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/tipsntrick.css">
	<script type="text/javascript" src="Assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="Assets/js/jquery.js"></script>
	<script type="text/javascript" src="Assets/js/tipsntrick.js"></script>
</head>
<body>
<div class="container" id="content">
	<div class="col-sm-8" id="left">
		<div class="row" id="judul">
			<div class="col-sm-2" id="logo">
				<img src="Assets/img/itb.png" id="avatar">
			</div>
			<div class="col-sm-10">
				<div class="row" id="atas">Judul trit</div>
				<div class="row" id="bawah">deskripsi</div>
			</div>
		</div>
		<div class="col-sm-2"></div>
		<div id="detail" class="row col-sm-10">Posted in: tanggal_sekarang</div>
	</div>
	<div class="col-sm-4" id="right"></div>
</div>
</body>
</html>