<html>
<head>
	<title>Register Form</title>
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/login_form.css">
	<script type="text/javascript" src="Assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="Assets/js/jquery.js"></script>
	<script type="text/javascript" src="Assets/js/login_form.js"></script>
</head>
<body>
<div class="container" id="front">
	<img src="Assets/img/itb.png" id="logo">
	<div>
		<?php echo $_GET['q']; ?>
	</div>
	<form role="form">
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password">
		</div>
		<button type="submit" id="button" class="btn btn-default" value="login">Login</button>
	</form>
</div>
</body>
</html>