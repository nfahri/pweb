<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*router the guardian*/
Route::get('/','DashboardController@index');
Route::get('/daftar','DashboardController@daftar');
Route::get('/login','DashboardController@login');
Route::get('/forum','ForumController@index');
Route::get('/tips-trik','TipsTrikController@index');

// Route::get('/{id}','DashboardController@welcome');
// Route::get('/{id}/forum','ForumController@index');
// Route::get('/{id}/flora','FloraController@index');
// Route::get('/{id}/fauna','FaunaController@index');
// Route::get('/{id}/event','EventController@index');
// Route::get('/{id}/artikel','ArtikelController@index');
// Route::get('/{id}/report','ReportController@index');
// Route::get('/{id}/donation','DonationController@index');

/*Route::get('/', 'WelcomeController@index');*/

// Route::get('/','PagesController@index');

// Route::get('/about','PagesController@about');

// Route::get('home', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

// Route::get('aku','AkuController@index');


/*Justin Routing*/
// Route::get('test','MeController@index');

// Route::get('test/{id}','MeController@about');