<html>
<head>
	<title>Register Form</title>
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="Assets/css/view_reg.css">
	<script type="text/javascript" src="Assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="Assets/js/jquery.js"></script>
	<script type="text/javascript" src="Assets/js/script_reg.js"></script>
</head>
<body>
<div class="container-fluid" id="back">
	<div class="container" id="front">
		<img src="Assets/img/itb.png" id="logo">
		<form role="form">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" id="username">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password">
			</div>
			<div class="form-group">
				<label for="cpassword">Confirm Password</label>
				<input type="password" class="form-control" id="cpassword">
			</div>
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" id="name">
			</div>
			<div class="form-group">
				<label for="date">Birth</label>
				<input type="date" class="form-control" id="date">
			</div>
			<button type="submit" id="button" class="btn btn-default" value="submit">Submit</button>
		</form>
	</div>
</div>
</body>
</html>