<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function index()
	{
		return view('home');
	}
	public function daftar(){
		return view('register_form');
	}
	public function login(){
		return view('login_form');
	}

	public function welcome($id)
	{
			return "ini buat bagian newsfeed yg udah login buat id ".$id;
	}
}
